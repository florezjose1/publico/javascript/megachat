const {app, BrowserWindow} = require('electron');
const path = require('path');
const url = require('url');

let win;

function createWindow() {
  console.log('Hola');
  win = new BrowserWindow({
    width: 800, height: 600,
    icon: __dirname + 'dist/megaChat/assets/img/Mega-Chat-Logo-Circulo.png'
  });
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'dist/megaChat/index.html'),
    protocol: 'file:',
    slashes: true,
  }));
  win.on('closed', () => {
    win = null;
  });
}

app.on('ready', createWindow);

app.on('window-all-closed', () => {
  console.log('Debuger')
  if (process.platform !== 'darwin') {
    app.quit()
  }
});

app.on('activate', () => {
  if (win === null) {
    createWindow()
  }
});
