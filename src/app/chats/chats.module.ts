import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ChatListComponent} from './chat-list/chat-list.component';
import {ChatRouterModule} from './chat.router';
import {
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatDialogModule,
  MatButtonModule,
} from '@angular/material';
import {environment} from '../../environments/environment';
import {MatChipsModule} from '@angular/material/chips';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {ChatComponent} from './chat/chat.component';
import {ContactsComponent} from './contacts/contacts.component';
import {NoticeComponent} from './notice/notice.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {FlexLayoutModule} from '@angular/flex-layout';
import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {DialogAddImageChatComponent} from './dialog-add-image-chat/dialog-add-image-chat.component';
import {LyIconModule} from '@alyle/ui/icon';
import {LyButtonModule} from '@alyle/ui/button';
import {LyResizingCroppingImageModule} from '@alyle/ui/resizing-cropping-images';
import {LyGridModule} from '@alyle/ui/grid';
import {LyFieldModule} from '@alyle/ui/field';

@NgModule({
  declarations: [ChatListComponent,
    ChatComponent,
    ContactsComponent,
    NoticeComponent,
    DialogAddImageChatComponent
  ],
  imports: [
    CommonModule,
    ChatRouterModule,
    MatCheckboxModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,

    FormsModule,
    ReactiveFormsModule,
    LyFieldModule,
    LyButtonModule,
    LyIconModule,
    LyResizingCroppingImageModule,
    LyGridModule,

    PerfectScrollbarModule,
    FlexLayoutModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  entryComponents: [
    NoticeComponent,
    DialogAddImageChatComponent
  ],
})
export class ChatsModule {
}
