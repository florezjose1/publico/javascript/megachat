import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DialogAddImageChatComponent } from './dialog-add-image-chat.component';

describe('DialogAddImageChatComponent', () => {
  let component: DialogAddImageChatComponent;
  let fixture: ComponentFixture<DialogAddImageChatComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DialogAddImageChatComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DialogAddImageChatComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
