import {RouterModule, Routes} from '@angular/router';
import {ChatListComponent} from './chat-list/chat-list.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {path: 'chat', component: ChatListComponent, data: {animation: 'chat'}}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ChatRouterModule {}
