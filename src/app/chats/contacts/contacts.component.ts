import { Component, Input, Output, EventEmitter } from '@angular/core';
import {User} from '../../interfaces/user';
import {Globals} from '../../globals';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.scss']
})
export class ContactsComponent {

  @Input() friends: User[];
  @Input() user;
  @Output() onActiveChat = new EventEmitter();

  constructor(public global: Globals) {
  }
  ngAfterViewInit() {
  }
  setActiveChat(chat) {
    this.onActiveChat.emit(chat);
  }

}
