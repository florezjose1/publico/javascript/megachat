import {Component, Input, SecurityContext, ViewChild} from '@angular/core';
import {MatDialog} from '@angular/material';
import {NoticeComponent} from '../notice/notice.component';
import {PerfectScrollbarComponent} from 'ngx-perfect-scrollbar';
import {isUndefined} from 'util';
import {ChatsService} from '../chats.service';
import {Globals} from '../../globals';
import {DomSanitizer} from '@angular/platform-browser';
import {DialogAddImageChatComponent} from '../dialog-add-image-chat/dialog-add-image-chat.component';
import {AngularFireStorage} from '@angular/fire/storage';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-chat',
  templateUrl: './chat.component.html',
  styleUrls: ['./chat.component.scss']
})
export class ChatComponent {

  @Input() user;
  @Input() chatSidenav;
  @Input() activeChat;
  @Input() conversation;
  @Input() conversation_id;
  @Input() messages;

  newMessage: string;
  imageURL: any = null;

  @ViewChild('scrollChatContent') scrollChatContent: PerfectScrollbarComponent;

  constructor(public global: Globals, public dialog: MatDialog, private chatService: ChatsService,
              private userService: UserService,
              private sanitizer: DomSanitizer, private firebaseStore: AngularFireStorage) {
    // this.scrollChatContent.directiveRef.scrollToBottom(0, 100);
  }

  getBackground(img) {
    return this.sanitizer.sanitize(SecurityContext.STYLE, 'url(' + img + ')');
  }

  scrollDownActive() {
    if (isUndefined(this.scrollChatContent)) {
      return;
    }
    this.scrollChatContent.directiveRef.scrollToBottom(0, 100);
  }

  onSendTriggered() {
    if (this.newMessage) {
      const message = {
        uid: this.conversation_id,
        timestamp: Date.now(),
        message: this.newMessage,
        sender: this.user.uid,
        receiver: this.activeChat.uid,
        type: 'text',
        imageURL: this.imageURL
      };
      this.chatService.createConversation(message).then(() => {
        this.scrollDownActive();
        this.newMessage = '';
      });
    }

  }

  clearMessages(activeChat) {
    activeChat.messages.length = 0;
  }

  onChatSideTriggered() {
    this.chatSidenav.toggle();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogAddImageChatComponent, {
      width: '650px',
      height: 'auto',
      data: {
        uid: this.conversation_id,
        timestamp: Date.now(),
        message: this.newMessage,
        sender: this.user.uid,
        receiver: this.activeChat.uid,
        type: 'text',
        imageURL: this.imageURL
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      // this.newMessage = result;
      console.log(result);

      if (result.imageURL) {
        const currentPictureId = Date.now();
        const picture = this.firebaseStore.ref('chats/chat/' + this.conversation_id + '/' + currentPictureId + '.jpg')
          .putString(result.imageURL, 'data_url');

        picture.then(() => {
          this.imageURL = this.firebaseStore.ref('chats/chat/' + this.conversation_id + '/' + currentPictureId + '.jpg').getDownloadURL();
          this.imageURL.subscribe(p => {
            console.log(p);
            result.imageURL = p;
            this.chatService.createConversation(result).then(() => {
              this.scrollDownActive();
              this.newMessage = '';
            });
          });
        }).catch(error => console.log(error));
      }
    });
  }

}
