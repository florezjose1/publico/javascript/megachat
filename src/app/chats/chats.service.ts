import {Injectable} from '@angular/core';
import {AngularFireDatabase} from '@angular/fire/database';

@Injectable({
  providedIn: 'root'
})
export class ChatsService {

  constructor(private angularFireDatabase: AngularFireDatabase) {
  }

  getUsers() {
    return this.angularFireDatabase.list('/users');
  }

  createChat(chat) {
    return this.angularFireDatabase.object('/chat/' + chat.uid + '/' + chat.timestamp).set(chat);
  }

  getChat(uid) {
    return this.angularFireDatabase.list('/chat/' + uid);
  }

  createConversation (conversation) {
    return this.angularFireDatabase.object('/conversations/' + conversation.uid + '/' + conversation.timestamp).set(conversation);
  }

  getConversation(uid) {
    return this.angularFireDatabase.list('/conversations/' + uid);
  }

  editConversation (conversation) {
    return this.angularFireDatabase.object('/conversations/' + conversation.uid + '/' + conversation.timestamp).set(conversation);
  }
}
