import {Component, OnInit, ViewChild} from '@angular/core';

import {User} from '../../interfaces/user';
import {ChatsService} from '../chats.service';
import {UserService} from '../../services/user.service';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-chat-list',
  templateUrl: './chat-list.component.html',
  styleUrls: ['./chat-list.component.scss']
})
export class ChatListComponent implements OnInit {
  friends: User[];
  activeChat: any;
  user: User;
  conversation_id: string;
  conversation: any[];


  constructor(private chatService: ChatsService, private userService: UserService,
              private authenticationService: AuthenticationService) {

    this.authenticationService.getStatusSession().subscribe(status => {
      this.userService.getUserById(status.uid).valueChanges().subscribe((data: User) => {
        this.user = data;
      }, error1 => console.log(error1));
    });

    this.chatService.getUsers().valueChanges().subscribe(
      (data: User[]) => {
        this.friends = data;
        // this.activeChat = data[0];
      }, (error) => console.log(error)
    );
  }

  ngOnInit() {
  }


  clearMessages(activeChat) {
    activeChat.messages.length = 0;
  }

  getConversation() {
    this.chatService.getConversation(this.conversation_id).valueChanges().subscribe(response => {
      this.conversation = response;
    }, error => console.log(error));
  }

  onActiveChat(chat) {
    this.activeChat = chat;
    if (this.activeChat) {
      const ids = [this.user.uid, this.activeChat.uid].sort();
      this.conversation_id = ids.join('|');
      this.getConversation();
    }
  }
}
