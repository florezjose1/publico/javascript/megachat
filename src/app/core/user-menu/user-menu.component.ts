import {Component, OnInit, Input, HostListener, ElementRef} from '@angular/core';
import {AuthenticationService} from '../../services/authentication.service';
import {Router} from '@angular/router';
import {Globals} from '../../globals';

@Component({
  selector: 'cdk-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {
  isOpen: boolean = false;

  // currentUser = null;
  Hari: string;

  @Input() currentUser = null;
  @Input() user;

  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) {
      return;
    }

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.isOpen = false;
    }
  }


  constructor(public global: Globals, private elementRef: ElementRef, private authenticationService: AuthenticationService, private router: Router) {
  }


  ngOnInit() {
  }

  logout() {
    this.authenticationService.logoutOut().then((response) => {
      this.router.navigate(['/']);
    }).catch(error => console.log(error));
  }

}
