import { Component, OnInit, Input } from '@angular/core';
import { menus } from './menu-element';
import {Globals} from '../../globals';

@Component({
  selector: 'cdk-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss']
})
export class SidemenuComponent implements OnInit {

  @Input() iconOnly:boolean = false;
  @Input() user: any;

  public menus = menus;

  constructor(public global: Globals) { }

  ngOnInit() {
  }

}
