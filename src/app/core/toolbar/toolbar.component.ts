import {Component, Input, OnInit} from '@angular/core';
import {ToolbarHelpers} from './toolbar.helpers';

@Component({
  selector: 'cdk-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {
  @Input() sidenav;
  @Input() sidebar;
  @Input() drawer;
  @Input() matDrawerShow;
  @Input() user;

  searchOpen: boolean = false;
  toolbarHelpers = ToolbarHelpers;

  constructor() {
  }

  ngOnInit() {
  }

}
