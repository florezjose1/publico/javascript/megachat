import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'cdk-sidemenu-item',
  templateUrl: './sidemenu-item.component.html',
  styleUrls: ['./sidemenu-item.component.scss']
})
export class SidemenuItemComponent implements OnInit {

  @Input() menu;
  @Input() iconOnly: boolean;
  @Input() secondaryMenu = false;

  constructor() { }

  ngOnInit() {
  }

  openLink() {
    this.menu.open = this.menu.open;
  }

  checkForChildMenu() {
    return !!(this.menu && this.menu.sub);
  }

}
