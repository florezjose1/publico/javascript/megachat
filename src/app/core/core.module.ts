import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {MatIconModule} from '@angular/material';
import {MatListModule} from '@angular/material/list';
import {MatChipsModule} from '@angular/material/chips';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {
  MatSidenavModule,
  MatSliderModule,
  MatProgressBarModule,
} from '@angular/material';

import {FlexLayoutModule} from '@angular/flex-layout';
import {MatToolbarModule} from '@angular/material/toolbar';

import {SidemenuComponent} from './sidemenu/sidemenu.component';
import {SidemenuItemComponent} from './sidemenu-item/sidemenu-item.component';

import {PERFECT_SCROLLBAR_CONFIG} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarConfigInterface} from 'ngx-perfect-scrollbar';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';
import {ToolbarComponent} from './toolbar/toolbar.component';
import {UserMenuComponent} from './user-menu/user-menu.component';
import {ToolbarNotificationComponent} from './toolbar-notification/toolbar-notification.component';
import {ChatsModule} from '../chats/chats.module';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatTabsModule} from '@angular/material/tabs';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};

@NgModule({
  declarations: [
    SidemenuComponent,
    SidemenuItemComponent,
    ToolbarComponent,
    UserMenuComponent,
    ToolbarNotificationComponent,
  ],
  imports: [
    CommonModule,
    ChatsModule,
    MatListModule,
    MatButtonModule,
    MatInputModule,
    MatIconModule,
    MatChipsModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    MatToolbarModule,
    MatSliderModule,
    MatProgressBarModule,
    MatSidenavModule,

    MatFormFieldModule,

    MatTabsModule,


    RouterModule,

    FlexLayoutModule,
    PerfectScrollbarModule,
  ],
  exports: [
    SidemenuComponent,
    SidemenuItemComponent,
    ToolbarComponent,
    UserMenuComponent,
    ToolbarNotificationComponent,
  ],
  providers: [
    {
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ]
})
export class CoreModule {
}
