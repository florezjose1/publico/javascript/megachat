import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {ProfileCardComponent} from './profile-card/profile-card.component';

const routes: Routes = [
  {path: '', component: ProfileCardComponent, data: {animation: 'chat'}}
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [
    RouterModule
  ]
})

export class ProfileRouterModule {}
