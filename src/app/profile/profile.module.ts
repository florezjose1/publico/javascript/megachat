import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  MatIconModule,
  MatListModule,
  MatMenuModule,
  MatSidenavModule,
  MatToolbarModule,
  MatInputModule,
  MatDialogModule,
  MatButtonModule, MatDatepickerModule, MatNativeDateModule,
} from '@angular/material';

import {environment} from '../../environments/environment';
import {MatChipsModule} from '@angular/material/chips';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';

import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {FlexLayoutModule} from '@angular/flex-layout';

import {AngularFireModule} from '@angular/fire';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireDatabaseModule} from '@angular/fire/database';

import {ProfileCardComponent} from './profile-card/profile-card.component';
import {ProfileRouterModule} from './profile.router';
import {LyFieldModule} from '@alyle/ui/field';
import {LyThemeModule} from '@alyle/ui';
import {LyGridModule} from '@alyle/ui/grid';
import {LyResizingCroppingImageModule} from '@alyle/ui/resizing-cropping-images';
import {LyIconModule} from '@alyle/ui/icon';
import {LyButtonModule} from '@alyle/ui/button';

@NgModule({
  declarations: [ProfileCardComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ProfileRouterModule,
    MatCheckboxModule,
    MatChipsModule,
    MatIconModule,
    MatListModule,
    MatMenuModule,
    MatSidenavModule,
    MatToolbarModule,
    MatInputModule,
    MatDialogModule,
    MatButtonModule,
    MatExpansionModule,
    MatDatepickerModule,
    MatNativeDateModule,

    LyThemeModule.setTheme('minima-light'),
    LyButtonModule,
    LyFieldModule,
    LyGridModule,
    LyIconModule,
    LyResizingCroppingImageModule,

    PerfectScrollbarModule,

    FlexLayoutModule,

    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFireDatabaseModule,
  ],
  entryComponents: [],
})
export class ProfileModule {
}
