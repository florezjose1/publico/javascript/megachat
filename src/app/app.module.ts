import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {CoreModule} from './core/core.module';
import {HttpClientModule} from '@angular/common/http';
import {LazyLoadModule} from './lazy-load/lazy-load.module';

import {environment} from '../environments/environment';
import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import {AngularFireAuthModule} from '@angular/fire/auth';
import {AngularFireStorageModule} from '@angular/fire/storage';
import {AngularFireDatabaseModule} from '@angular/fire/database';
import {Globals} from './globals';

import {
  LyThemeModule,
  LyCommonModule,
  PartialThemeVariables,
  LY_THEME,
  LY_THEME_GLOBAL_VARIABLES
} from '@alyle/ui';

import { MinimaLight } from '@alyle/ui/themes/minima';

export class CustomMinimaLight extends MinimaLight {
  primary = {
    default: '#0077D5',
    contrast: '#fff'
  };
  accent = {
    default: '#e91e63',
    contrast: '#fff'
  };
}


@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    HttpClientModule,
    LazyLoadModule,
    LyThemeModule.setTheme('minima-light'),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    AngularFireDatabaseModule
  ],
  providers: [{ provide: LY_THEME, useClass: CustomMinimaLight, multi: true }, Globals],
  bootstrap: [AppComponent]
})
export class AppModule { }
