export interface User {
  nick: string;
  age: number;
  email: string;
  friend: boolean;
  uid: any;
  status?: boolean;
  avatar?: string;
  messages?: any;
  birthday?: string;
  country?: string;
}
