import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {LyAvatarModule} from '@alyle/ui/avatar';
import {LyCardModule} from '@alyle/ui/card';
import {LyFieldModule} from '@alyle/ui/field';
import {LyGridModule} from '@alyle/ui/grid';
import {LyThemeModule} from '@alyle/ui';
import {LyButtonModule} from '@alyle/ui/button';
import {LyIconModule} from '@alyle/ui/icon';
import {LyTypographyModule} from '@alyle/ui/typography';
import {FlexLayoutModule} from '@angular/flex-layout';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LoginComponent} from './login.component';
import {PerfectScrollbarModule} from 'ngx-perfect-scrollbar';

const routes: Routes = [
  {path: '', component: LoginComponent},
];

@NgModule({
  declarations: [LoginComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    LyThemeModule.setTheme('minima-light'),
    LyAvatarModule,
    LyButtonModule,
    LyCardModule,
    LyFieldModule,
    LyGridModule,
    LyIconModule,
    LyTypographyModule,
    PerfectScrollbarModule
  ],
  exports: [
    RouterModule
  ],
})
export class LoginModule { }
