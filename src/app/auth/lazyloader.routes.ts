import {Routes} from '@angular/router';
import {AuthComponent} from './auth.component';

export const appRoutes: Routes = [{
  path: '', component: AuthComponent, children: [
    {path: 'chats', loadChildren: '../chats/chats.module#ChatsModule'},
    {path: 'profile', loadChildren: '../profile/profile.module#ProfileModule'},
  ]
}];
